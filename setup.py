from setuptools import setup, find_packages

setup(
    name='LbConfiguration',
    packages=find_packages(),
    version='0.1.0',    
    description='LbConfiguration. Here only used for creating doxygen configuration',
    url='https://github.com/shuds13/pyexample',
    author='Piet Nogga',
    author_email='piet.nogga@cern.ch',
    license='BSD 2-clause',
    install_requires=[                     
                      ],

    classifiers=[
        'Development Status :: 1 - Planning',
        'Intended Audience :: Science/Research',
        'Operating System :: POSIX :: Linux',        
        'Programming Language :: Python :: 3.9',
    ],
)
